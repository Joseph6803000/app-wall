Collections = {};


Collections.Messages = new Mongo.Collection('messages');


var imageStore = new FS.Store.GridFS("images");


Collections.Images = new FS.Collection("images", {
    stores: [imageStore]
});