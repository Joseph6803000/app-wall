"use strict";

Template.posts.helpers({
    postList: function () {
        return Collections.Messages.find({});
    },

    displayImages: function () {
        var postImage = Collections.Images.find({
            _id: this.imageId
        }).fetch();
        return postImage[0].url();
    },
    updateMasonry: function () {
        $('.grid').imagesLoaded().done(function () {
            $('.grid').masonry({
                itemSelector: '.grid-item',
                columWidth: '.grid-sizer',
                percentPosition: true
            });
        });

    }



});