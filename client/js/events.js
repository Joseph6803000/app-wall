Template.form.events({
    'submit .add-new-post': function (event) {
        event.preventDefault();

        var userName = event.currentTarget.children[0].value;

        var message = event.currentTarget.children[1].value;

        var postImage = event.currentTarget.children[2].files[0];

        if (userName === "") {
            alert("failed post");
            return false;
        }

        Collections.Images.insert(postImage, function (error, fileObject) {
            if (error) {
                alert("there has been an uploading error please try again");
                /*to-do: inform the user that the upload has failed*/
            } else {
                alert("file upload succesful");
                /*to-do: tell the user that it was succesful*/
                /*insert data: name, message, imageId, created by date*/
                /*fileObject._id*/
                Collections.Messages.insert({
                    name: userName,
                    createdAt: new Date(),
                    Text: message,
                    imageId: fileObject._id


                });


                $('.grid').masonry('reloadItems');
            }
        });
    }

});